#!/bin/bash

function get_latest_url() {
  if [[ "$1" =~ https://wfwf[0-9]*\.com ]]
  then
    echo $(curl --max-time 5 -k "$1" | strings | grep "https://wfwf[0-9]*\.com" | head -1 | awk -F'"' '{print $2}')
  elif [[ "$1" =~ https://hudu[0-9]*\.net ]]
  then
    echo $(curl --max-time 5 -k "$1" | strings | grep "https://hodu[0-9]*\.net" | head -1 | awk -F'"' '{print $2}')
  else
    echo $(curl --max-time 5 -k -D - -o /dev/null "$1" | awk -v FS=": " ' /^location|^Location/{print substr($2, 1, length($2)-1)}' | awk -F/ '{print "https://"$3}')
  fi
}

function select_url() {
  if [[ "$1" == "https://" ]]
  then
    echo $2
  else
    echo $1
  fi
}

IFS=$'\n' read -d '' -r -a lines < ./current.txt

rm -rf ./_redirects
rm -rf ./old.txt
mv ./current.txt ./old.txt

curl -i -k https://newtoki101.com
curl -i -k https://hodu217.net

for (( i=0; i<${#lines[@]}; i++ ));
do
  IFS='|' read -r -a path_url <<< "${lines[$i]}"

  result=$(get_latest_url ${path_url[1]})
  
  echo "response url: [$result]"
  
  latest_url=$(select_url $result ${path_url[1]})

  echo "latest url: [$latest_url]"

  echo "${path_url[0]}    $latest_url    301" >> ./_redirects 
  echo "${path_url[0]}|$latest_url" >> ./current.txt
done
